package framework;

class DynamicScene extends h2d.Scene {
    private var updaters: List<Updater>;
    private var sceneInteractor: h2d.Interactive;

    private function new(app: hxd.App) {
        super();
        updaters = new List();
        sceneInteractor = new h2d.Interactive(this.width, this.height, this);
        sceneInteractor.onKeyDown = function(e: hxd.Event) {
            switch e.keyCode {
                case hxd.Key.ESCAPE: Sys.exit(0);
                case hxd.Key.R: this.reset();
            }
        }
        #if debug
        new CompileLabel(app, this);
        #end 
    }

    private function reset(): Void {
    }

    public dynamic function update(dt: Float):Void {
        for (updater in updaters) {
            updater.update(dt);
        }
    }

    public function registerUpdater(u: Updater):Void {
        updaters.add(u);
    }

    public function removeUpdater(u: Updater):Bool {
        return updaters.remove(u);
    }
}