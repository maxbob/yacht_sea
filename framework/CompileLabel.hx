package framework;

import hxd.res.DefaultFont;

class CompileLabel extends h2d.Text implements Updater {

    var app: hxd.App;
    var elapsed: Float = 0;
    var updateCalls: Int = 0;

    public function new(app: hxd.App, parent: DynamicScene) {
        super(DefaultFont.get(), parent);
        this.text = "debug";
        this.app = app;
        parent.registerUpdater(this);
    }

    public function update(dt: Float):Void {
        updateCalls++;
        elapsed += dt;
        this.text = "debug, fps:" + Std.int(updateCalls / elapsed) + " engine.draw:" + app.engine.drawCalls;
        if (elapsed > 3) {
            updateCalls = 0;
            elapsed = 0;
        }
    }
}