package framework;

interface Updater {
    public function update(dt: Float):Void;
}
