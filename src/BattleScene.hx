import framework.*;

class BattleScene extends DynamicScene {
    var pUI: PlayerUI;
    public function new(game: Main) {
        super(game);
        pUI = new PlayerUI(this);
    }

    override function reset(): Void {
        this.pUI.reset();
    }
}

// Player UI contains the bank, tray, and roll button. Should take up roughly the bottom 3/5 of the screen...?
class PlayerUI {
    public static inline var UIBuffer: Float = 20;
    public static inline var RollText: String = "ROLL";
    public static inline var StashText: String = "STASH";

    private var uiHeight: Float;
    private var uiWidth: Float;
    private var uiTray: h2d.Graphics;

    private var rollCount: Int;

    public var rollButton: Button;
    public var diceTray: Array<Die>;
    public var stashArray: Array<Stash>;

    public function new(parent: DynamicScene) {
        uiHeight = parent.height / 5 * 3;
        uiWidth = parent.width;
        uiTray = new h2d.Graphics(parent);
        uiTray.setPosition(0, parent.height - uiHeight);
        uiTray.beginFill();
        uiTray.setColor(0x92FFD8, 0.5);
        uiTray.drawRect(0, 0, uiWidth, uiHeight);
        uiTray.endFill();
        
        stashArray = initStashArray();
        diceTray = initDiceTray();
        rollButton = initButton();
    }

    public function reset(): Void {
        this.rollButton.enable();
        this.rollButton.setText(RollText);
    }

    private function initButton(): Button {
        var b = new Button(
            RollText,
            3.0,
            {x: 0, y: 0},
            uiTray);
        b.setPosition({x: uiWidth - b.outerArea.x - UIBuffer, y: uiHeight - b.outerArea.y - UIBuffer});
        rollCount = 0;

        var bInteractor = new h2d.Interactive(0,0);
        bInteractor.onClick = function(e: hxd.Event) {
            rollCount++;
            this.rollDice();
            if (rollCount > 2) {
                rollCount = 0;
                b.setText(StashText);
                b.disable();
                return;
            }
        }
        b.setInteractor(bInteractor);
        return b;
    }

    private function initDiceTray(): Array<Die> {
        var arr = new Array();
        for (i in 1...6) {
            arr.push(new Die({x: (Die.TotalPx + 5) * i - Die.TotalPx / 2, y: uiHeight - UIBuffer - 50}, uiTray));
        }
        return arr;
    }

    private function initStashArray(): Array<Stash> {
        var arr = new Array();
        for (i in 1...6) {
            arr.push(new Stash({x: UIBuffer, y: (Stash.TotalPx + 5) * i}, uiTray));
        }
        return arr;

    }

    private function rollDice() {
        for (d in diceTray) {
            d.roll();
        }
    }
}

class Die implements framework.Updater {
    public static inline var BorderColor = 0x000000;
    public static inline var HighlightedBorderColor = 0xF0E68C;
    public static inline var DieColor = 0xF08B8B;

    public static inline var BorderPx = 3;
    public static inline var SidePx = 50;
    public static inline var TotalPx = SidePx + 2 * BorderPx;

    public var canRoll: Bool;
    private var gfx: h2d.Graphics;
    private var gfxSize: framework.Point;
    private var valText: h2d.Text;
    private var val: Int;

    public function new(position: framework.Point, parent: h2d.Object) {
        canRoll = true;
        gfx = new h2d.Graphics(parent);
        gfx.setPosition(position.x, position.y);
        valText = new h2d.Text(hxd.res.DefaultFont.get(), gfx);
        valText.setScale(3);
        randomizeVal();
        valText.setPosition((TotalPx - this.valText.textWidth * this.valText.scaleX) / 2,
                            (TotalPx - this.valText.textHeight * this.valText.scaleY) / 2);
        draw();
        var interactor = new h2d.Interactive(TotalPx, TotalPx, gfx);
        interactor.onClick = toggle;
    }

    private function draw() {
        if (canRoll) {
            gfx.beginFill(BorderColor);
        } else {
            gfx.beginFill(HighlightedBorderColor);
        }
        gfx.drawRoundedRect(0, 0, TotalPx, TotalPx, BorderPx);
        gfx.setColor(DieColor);
        gfx.drawRect(BorderPx, BorderPx, SidePx, SidePx);
        gfx.endFill();
    }

    private function randomizeVal() {
        val = 1 + Std.int(Math.random() * 5);
        valText.text = "" + val;
    }

    public function roll() {
        if (canRoll) {
            this.randomizeVal();
        }
    }

    private function toggle(e: hxd.Event) {
        canRoll = !canRoll;
        draw();
    }

    public function update(dt: Float) {}
}

class Stash implements framework.Updater {
    public static inline var BgColor = Button.BgColor;
    public static inline var CrossColor = 0x000000;
    public static inline var DisabledColor = Button.DisabledColor;
    public static inline var HoveredColor = Button.HoveredColor;
    public static inline var NonHoveredColor = Button.NonHoveredColor;

    public static inline var BorderPx = 3;
    public static inline var ButtonPx = 25;
    public static inline var CrossPx = BorderPx;
    public static inline var TotalPx = ButtonPx + 2 * BorderPx;

    private var gfx: h2d.Graphics;

    public function new(position: framework.Point, parent: h2d.Object) {
        gfx = new h2d.Graphics(parent);
        gfx.setPosition(position.x, position.y);
        draw();
    }

    private function draw(): Void {
        gfx.beginFill(BgColor);
        gfx.drawRoundedRect(0, 0, TotalPx, TotalPx, BorderPx);
        gfx.setColor(NonHoveredColor);
        gfx.drawRoundedRect(BorderPx, BorderPx, ButtonPx, ButtonPx, BorderPx);
        gfx.setColor(CrossColor);
        gfx.drawRect((TotalPx - CrossPx) / 2, 3 * BorderPx, CrossPx, TotalPx - 6 * BorderPx);
        gfx.drawRect(3 * BorderPx, (TotalPx - CrossPx) / 2, TotalPx - 6 * BorderPx, CrossPx);
        gfx.endFill();
    }

    public function update(dt: Float) {}
}