class Button {
    public static inline var BgColor = 0x000000;
    public static inline var DisabledColor = 0x9e9e9e;
    public static inline var NonHoveredColor = 0x19AEA5;
    public static inline var HoveredColor = 0x46beb7;
    
    public static inline var BorderPx = 5;
    public static inline var TextBorderPx = 50;
    public static inline var ButtonRadius = 5;

    private var bText: h2d.Text;
    private var interactor: h2d.Interactive;
    private var inner: h2d.Graphics;
    private var innerArea: framework.Point;
    private var outer: h2d.Graphics;
    public var outerArea: framework.Point;
    private var sizeConst: framework.Point;

    public function new(
        buttonText: String,
        size: Float,
        position: framework.Point,
        parent: h2d.Object,
        ?interactor: h2d.Interactive
    ) {
        // Create text
        bText = new h2d.Text(hxd.res.DefaultFont.get());
        bText.text = buttonText;
        bText.setScale(size);
        sizeConst = {x: bText.textWidth * bText.scaleX, y: bText.textHeight * bText.scaleY};

        // Calculate the button size based on text size
        innerArea = {
            x: sizeConst.x + 2 * TextBorderPx,
            y: sizeConst.y + 2 * TextBorderPx / 3
        };

        outerArea = {
            x: innerArea.x + 2 * BorderPx,
            y: innerArea.y + 2 * BorderPx
        }

        this.bText.setPosition(
            (outerArea.x - sizeConst.x) / 2,
            (outerArea.y - sizeConst.y) / 2);

        // Create button graphics now
        outer = new h2d.Graphics(parent);
        outer.setPosition(position.x - outerArea.x / 2, position.y - outerArea.y / 2);
        outer.beginFill(BgColor);
        outer.drawRoundedRect(
            0,
            0,
            outerArea.x,
            outerArea.y,
            ButtonRadius);
        outer.endFill();
        this.setColor(NonHoveredColor);
        // Set up hierarchy
        outer.addChild(bText);

        if (interactor != null) {
            this.setInteractor(interactor);
        }
    }

    public function setInteractor(interactor: h2d.Interactive): Void {
        interactor.width = outerArea.x;
        interactor.height = outerArea.y;
        outer.addChild(interactor);

        interactor.onOver = function(e : hxd.Event) {
            this.setColor(HoveredColor);
        }

        interactor.onOut = function(e : hxd.Event) {
            this.setColor(NonHoveredColor);
        }

        this.interactor = interactor;
    }

    public function setText(text: String): Void {
        this.bText.text = text;
        // rescale to fit everything in the button
        var newScale = Math.min(sizeConst.x / this.bText.textWidth, sizeConst.y / this.bText.textHeight);
        this.bText.setScale(newScale);
        // recenter the text in the button
        this.bText.setPosition(
            (outerArea.x - this.bText.textWidth * this.bText.scaleX) / 2,
            (outerArea.y - this.bText.textHeight * this.bText.scaleY) / 2);
    }

    public function getText(): String {
        return this.bText.text;
    }

    public function setPosition(newPos: framework.Point): Void {
        this.outer.setPosition(newPos.x, newPos.y);
    }
            
    private function setColor(color: Int): Void {
        outer.beginFill(color);
        outer.drawRoundedRect(
            BorderPx,
            BorderPx,
            innerArea.x,
            innerArea.y,
            ButtonRadius
        );
        outer.endFill();
    }

    public function enable(): Void {
        outer.addChild(this.interactor);
        this.setColor(NonHoveredColor);
    }

    public function disable(): Void {
        outer.removeChild(this.interactor);
        this.setColor(DisabledColor);
    }
}