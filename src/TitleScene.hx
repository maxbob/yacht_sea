import hxd.res.DefaultFont;
import h2d.Text;

import framework.*;

class TitleScene extends DynamicScene {
    var game : Main;
    var title : Title;
    var startButton : Button;

    public function new(game : Main) {
        super(game);

        title = new Title(this);

        var buttonInteractor = new h2d.Interactive(0, 0);
        buttonInteractor.onClick = function(e: hxd.Event) {
            game.transitionToGame();
        }
        startButton = new Button(
            "Avast ye!!",
            2.0,
            {x: this.width / 2, y: this.height /4 * 3},
            this,
            buttonInteractor
        );
    }
}

class Title implements Updater {
    public var text: Text;

    private var origin : Point;
    private var elapsed : Float;

    public function new(parent: DynamicScene) {
        origin = {
            x: parent.width / 2,
            y: parent.height / 4
        };

        text = new Text(DefaultFont.get(), parent);
        text.text = "Yacht@sea";
        text.setScale(5.0);
        text.dropShadow = {dx: 1.0, dy: 1.0, color: 0x000000, alpha: 1.0};
        text.textAlign = Center;
        text.x = origin.x;
        text.y = origin.y;

        parent.registerUpdater(this);
    }

    public function update(dt: Float):Void {
        elapsed += dt;
        if (elapsed > 2 * Math.PI) {
            elapsed = 0;
        }
        text.y = origin.y + (origin.y / 6 * Math.sin(elapsed));
        text.rotation = Math.cos(elapsed) / 6;
    }
}

