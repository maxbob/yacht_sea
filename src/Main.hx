import h2d.Scene;
import framework.DynamicScene;
import TitleScene.TitleScene;

class Main extends hxd.App {
    var currentScene : DynamicScene;

    var ctr : Int;
    var elapsed : Float;

    override function init() {
        engine.backgroundColor = 0x282828;
        transitionToTitle();
    }

    public function transitionToTitle() {
        currentScene = new TitleScene(this);
        this.setScene(currentScene, true);
    }

    public function transitionToGame() {
        currentScene = new BattleScene(this);
        this.setScene(currentScene, true);
    }

    override function update(dt: Float) {
        currentScene.update(dt);
    }

    static function main() {
        new Main();
    }
}