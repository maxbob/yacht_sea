# yacht_sea

Game written in Haxe using Heaps. 

Mostly a project to learn the game development life-cycle.

## Dependencies
```
C:\Users\Max\Desktop\Projects\yacht_sea>haxe --version
4.0.0-rc.2+77068e10c

C:\Users\Max\Desktop\Projects\yacht_sea>haxelib version
3.4.0

C:\Users\Max\Desktop\Projects\yacht_sea>haxelib list
format: [3.4.2]
heaps: [1.6.1]
hldx: [1.10.0]
hlopenal: [1.5.0]
hlsdl: [1.10.0]

C:\Users\maxmo\Desktop\Projects\yacht_sea>hl --version
1.10.0
```

Yep, I develop on Windows lol